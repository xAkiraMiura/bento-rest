import { Entity, Plugin, PluginAPI, Variable } from '@ayanaware/bento';

import { Context, Next } from 'koa';
import * as Router from '@koa/router';

import { BentoREST } from '../BentoREST';
import { BentoRESTVariable } from '../BentoRESTVariable';
import { RESTEvent } from '../constants';
import { getRoutes, getRouterOptions, getRouterMiddleware } from '../decorators/internal';

import { Logger } from '@ayanaware/logger-api';
const log = Logger.get();

export class RouteManager implements Plugin {
	public name = '@ayanaware/bento-rest:RouteManager';
	public api!: PluginAPI;

	private routers: Map<string, Router> = new Map();

	private bentoREST: BentoREST;

	@Variable({ name: BentoRESTVariable.BENTOREST_TRACE, default: true })
	private readonly trace: boolean;

	public async onLoad() {
		const bentoREST = this.api.getPlugin(BentoREST);
		this.bentoREST = bentoREST;

		bentoREST.app.use(this.middleware.bind(this));
	}

	public async onPreComponentLoad(entity: Entity) {
		try {
			await this.addRoutes(entity);
		} catch (e) {
			log.warn(e);
		}
	}

	public async onPreComponentUnload(entity: Entity) {
		try {
			await this.removeRoutes(entity.name);
		} catch (e) {
			log.warn(e);
		}
	}

	public async addRoutes(entity: Entity) {
		const routes = getRoutes(entity);
		if (routes.length < 1) return;

		const name = entity.name;
		let middlewareCount = 0;
		let routeCount = 0;

		// Router
		let router = this.routers.get(name);
		if (!router) {
			const options = getRouterOptions(entity);

			router = new Router(options);
			this.routers.set(name, router);
		}

		// Router Middleware
		const middlewares = getRouterMiddleware(entity).sort((a, b) => a.priority - b.priority);
		for (const middleware of middlewares) {
			router.use(middleware.use.bind(entity));

			middlewareCount++;
		}

		// Router Routes
		for (const route of routes) {
			const use = async (ctx: Context) => {
				if (this.trace) {
					ctx.set('X-Component-Name', name);
					if (route.use.name) ctx.set('X-Component-Method', route.use.name);
				}

				const start = Date.now();
				try {
					await route.use.call(entity, ctx);
				} catch (e) {
					log.error(`Entity[${name}]: ${ctx.method} ${ctx.url} -> ${route.use.name}(): ${e}`);
					this.api.emit(RESTEvent.ERROR, {...route, entity: name }, ctx, e);

					throw e;
				}

				const ms = Date.now() - start;
				log.info(`Entity[${name}]: ${ctx.method} ${ctx.url} - ${ctx.status} ${ms}ms`);
			};

			router[route.method](route.path, use);
			routeCount++;

			const method = route.method.toUpperCase();
			const layer = router.stack[router.stack.length - 1];

			log.debug(`Entity[${name}]: ${method} ${layer.path} -> ${route.use.name}()`);
		}

		log.info(`Entity[${name}]: Registered "${routeCount}" routes, "${middlewareCount}" route middleware`);
	}

	public async removeRoutes(entityName: string) {
		if (!this.routers.has(entityName)) return;

		this.routers.delete(entityName);
	}

	public async middleware(ctx: Context, next: Next) {
		const middlewares = Array.from(this.routers.values()).map(router => router.middleware());

		if (middlewares.length > 0) {
			// primary router options support
			const router = new Router(this.bentoREST.routerOptions);
			middlewares.forEach(m => router.use(m));

			await router.middleware()(ctx as any, next);
		} else return next();
	}
}
