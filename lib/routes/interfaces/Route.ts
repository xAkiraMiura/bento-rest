import { Context, Next } from 'koa';

import { RESTMethod } from '../constants';

export type RoutePath = string | RegExp | Array<string | RegExp>;

export interface Route {
	method: RESTMethod;
	path: RoutePath;

	use: (ctx: Context) => Promise<any>;
}
