import * as Koa from 'koa';
import { MetadataKeys } from './internal';

export interface RouterMiddleware {
	priority: number;
	use: (ctx: Koa.Context, next: Koa.Next) => Promise<any>;
}

export function getRouterMiddleware(target: any) {
	const middleware: Array<RouterMiddleware> = Reflect.getMetadata(MetadataKeys.ROUTER_MIDDLEWARE, target.constructor) || [];
	if (!Array.isArray(middleware)) return [];

	return middleware;
}

export function RouterMiddleware(priority: number): MethodDecorator {
	return (target: any, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<any>) => {
		if (target.proptotype === undefined) target = target.constructor;

		const middleware: Array<RouterMiddleware> = Reflect.getMetadata(MetadataKeys.ROUTER_MIDDLEWARE, target) || [];
		middleware.push({ priority, use: descriptor.value });

		Reflect.defineMetadata(MetadataKeys.ROUTER_MIDDLEWARE, middleware, target);
	};
}
