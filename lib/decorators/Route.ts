import { RESTMethod, Route, RoutePath } from '../routes';
import { MetadataKeys } from './internal';

export function getRoutes(target: any) {
	const routes: Array<Route> = Reflect.getMetadata(MetadataKeys.ROUTE, target.constructor) || [];
	if (!Array.isArray(routes)) return [];

	return routes;
}

export function ROUTE(method: RESTMethod, path: RoutePath): MethodDecorator {
	return (target: any, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<any>) => {
		if (target.prototype === undefined) target = target.constructor;
		const middleware: Array<Route> = Reflect.getMetadata(MetadataKeys.ROUTE, target) || [];

		middleware.push({ path, method, use: descriptor.value });

		Reflect.defineMetadata(MetadataKeys.ROUTE, middleware, target);
	};
}

export function GET(path: RoutePath) {
	return ROUTE(RESTMethod.GET, path);
}

export function POST(path: RoutePath) {
	return ROUTE(RESTMethod.POST, path);
}

export function DELETE(path: RoutePath) {
	return ROUTE(RESTMethod.DELETE, path);
}

export function PUT(path: RoutePath) {
	return ROUTE(RESTMethod.PUT, path);
}

export function PATCH(path: RoutePath) {
	return ROUTE(RESTMethod.PATCH, path);
}
