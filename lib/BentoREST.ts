import * as path from 'path';

import { FSEntityLoader, Plugin, PluginAPI, Variable } from '@ayanaware/bento';

import * as Router from '@koa/router';
import * as Koa from 'koa';

import { BentoRESTVariable } from './BentoRESTVariable';
import { RESTEvent } from './constants';
import { MiddlewareManager } from './middleware';
import { RouteManager } from './routes';

import { Logger } from '@ayanaware/logger-api';
const log = Logger.get('BentoREST');

export class BentoREST implements Plugin {
	public readonly name = '@ayanaware/bento-rest';
	public readonly version: string = null;
	public readonly api: PluginAPI;

	public app: Koa;
	public routerOptions: Router.RouterOptions = {};

	private fsLoader: FSEntityLoader;

	@Variable({ name: BentoRESTVariable.BENTOREST_PORT, default: 3000 })
	public port: number;
	@Variable({ name: BentoRESTVariable.BENTOREST_TRACE, default: true })
	private readonly trace: boolean;

	public constructor(options: Router.RouterOptions = {}) {
		this.routerOptions = options;

		try {
			const { version } = require('../package.json');
			this.version = version;
		} catch (e) {
			this.version = 'error';
			log.error(`Failed to load version information: ${e}`);
		}
	}

	public async onLoad() {
		// Create Own FSLoader instance
		this.fsLoader = new FSEntityLoader();
		this.fsLoader.name = '@ayanaware/bento-rest:FSEntityLoader';
		await this.api.bento.addPlugin(this.fsLoader)

		this.app = new Koa();
		this.app.use(async (ctx, next) => {
			if (this.trace) {
				ctx.set('X-Bento-Version', this.api.getBentoVersion());
				ctx.set('X-BentoREST-Version', this.version);
			}

			return next();
		});

		const middlewareManager: MiddlewareManager = await (this.fsLoader as any).createInstance(path.resolve(__dirname, 'middleware'));
		await this.api.bento.addPlugin(middlewareManager);

		const routeManager: RouteManager = await (this.fsLoader as any).createInstance(path.resolve(__dirname, 'routes'));
		await this.api.bento.addPlugin(routeManager);

		this.app.listen(this.port, () => {
			this.api.emit(RESTEvent.LISTENING, this.app);
			log.info(`Server Listening on 0.0.0.0:${this.port}`);

			return;
		});
	}
}
