import { Context, Next } from 'koa';

export interface Middleware {
	name: string;
	priority: number;

	use: (ctx: Context, next: Next) => Promise<any>;
}
