import { Entity } from '@ayanaware/bento';

import { Context, Next } from 'koa';

export interface MiddlewareEntity extends Entity {
	parent: Entity;

	priority: number;
	use: (ctx: Context, next: Next) => Promise<any>;
}
