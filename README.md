# @ayanaware/bento-rest

A [Bento](https://gitlab.com/ayanaware/bento) Plugin that makes building REST APIs easy. BentoREST uses [koa](https://www.npmjs.com/package/koa) and [@koa/router](https://www.npmjs.com/package/@koa/router) behind the scenes.

## BentoREST Variables
Bento Variables are used to change the behavior of BentoREST. They can be set various different ways.
Some common ways are to use Bento's built in `VariableLoader` or `VariableFileLoader` plugins.
You can also bring your own plugin or alternativly directly inject values via `Bento.setVariable()`.

>Note: While some variables are dynamic and BentoREST reacts as soon as you update them. Some are not.
This means BentoREST expects you to set all Variables you care about before `Bento.addPlugin()

Key | Type | Description | Default
--- | --- | --- | ---
BENTOREST_PORT | number | Port to listen on | 3000
BENTOREST_TRACE | boolean | Add BentoREST `X-` headers. ie: `X-Bento-Version`, `X-Component-Name` | true

## Decorators
BentoREST uses Typescript Decorators to build your REST application.

### `@Router(options)`
The Router decorator should be used on Bento Entities. It's optional and allows you to define RouterOptions on a per-entity Basis.

### `@Middleware(priority)`
The Middleware decorator allows you to add per-entity Koa middleware. If used a priority must be specififed. Priority determines the order that middleware will take effect. The smallest number is ran first. Followed by the next smallest. And so on

### `@Map(method, path)`
The Map decorator is used to define routes. There is also some convince decorators such as `@GET(path)`, `@POST(path)`, `@DELETE(path)`. These simply alias `@Map` behind the scenes.

## Contributers

HcgRandon  
TheAkio  
MeLike2D