import { Bento } from '@ayanaware/bento';
import { BentoREST, GET, Router, RouterMiddleware } from '@ayanaware/bento-rest';
import { Context, Next } from 'koa';

@Router({ prefix: '/hello' })
class Basic {
	public name = 'basic';

	@RouterMiddleware(0)
	private async coolMiddleware(ctx: Context, next: Next) {
		ctx.set('X-Cool', 'true');
		return next();
	}

	@GET('/world')
	private async getWorld(ctx: Context) {
		ctx.body = 'Hello World';
	}

	@GET('/error')
	private async getError() {
		throw new Error('Uh Oh');
	}
}

(async () => {
	const bento = new Bento();
	const rest = new BentoREST({ prefix: '/test' });

	await bento.addPlugins([rest]);

	await bento.addComponent(new Basic());

	await bento.verify();
})().catch(e => {
	console.log(e);
	process.exit(1);
});